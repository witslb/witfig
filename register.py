import yaml
import json
import sys
import bcrypt
import random
import string
import os
from pathlib import Path

if not os.path.exists("secrets.json"):
        with open("secrets.json", "w") as f:
            f.write("{}")

def generate_random_secret(length: int) -> str:
    characters = string.ascii_letters + string.digits
    return ''.join(random.choice(characters) for _ in range(length))

def generate_hashed_secret(id: str, secret: str) -> str:
    salt = bcrypt.gensalt()
    combined_secret = id + secret
    hashed_secret = bcrypt.hashpw(combined_secret.encode('utf-8'), salt)
    return hashed_secret.decode('utf-8')

def load_secrets():
    with open("secrets.json") as f:
        return json.load(f)

def update_secrets_file(id: str, hashed_secret: str) -> None:
    secrets = load_secrets()
    secrets[id] = hashed_secret

    with open("secrets.json", "w") as f:
        json.dump(secrets, f, indent=2)

def create_sample_config_file(id: str) -> None:
    config_path = Path(f"config_storage/{id}.yaml")
    config_path.parent.mkdir(parents=True, exist_ok=True)

    sample_config = {"hello": "world"}

    with open(config_path, "w") as f:
        yaml.dump(sample_config, f, default_flow_style=False)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python generate_secret.py [ID]")
        sys.exit(1)

    id = sys.argv[1]
    config_path = Path(f"config_storage/{id}.yaml")

    if config_path.is_file():
        print(f"Warning: Config file for ID '{id}' already exists.")

    secrets = load_secrets()

    if id in secrets:
        print(f"Error: ID '{id}' already exists in secrets.json file.")
        sys.exit(1)

    secret = generate_random_secret(32)
    hashed_secret = generate_hashed_secret(id, secret)
    print(f"ID: {id}")
    print(f"Generated Secret: {secret}")
    print(f"Hashed Secret: {hashed_secret}")
    update_secrets_file(id, hashed_secret)
    print("Updated secrets.json file.")
    if not config_path.is_file():
        create_sample_config_file(id)
        print(f"Created sample config file '{id}.yaml'.")
