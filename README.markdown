# Intro

Central config storage server, with read-only API. 

# Usage

1. Execute `witfig.py` to start API server. 
2. Use `register.py` to create config credentials
3. Edit yaml config file manually
4. Use ID + secret to call API e.g. `/api/get_config?id=prod/mainapi` with request headers
