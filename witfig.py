import json
import bcrypt
import logging
from fastapi import FastAPI, Query
from fastapi.responses import JSONResponse
from pathlib import Path

# Set up logging
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("app.log"),
        logging.StreamHandler()
    ]
)

app = FastAPI()

def load_secrets():
    with open("secrets.json") as f:
        return json.load(f)

@app.get("/api/yaml/get")
async def get_config(id: str = Query(...), secret: str = Query(...)):
    try:
        secrets = load_secrets()

        if id in secrets:
            secret_hash = secrets[id].encode('utf-8')
            combined_secret = id + secret
            secret_bytes = combined_secret.encode('utf-8')

            if bcrypt.checkpw(secret_bytes, secret_hash):
                config_path = Path(f"config_storage/{id}.yaml")

                if config_path.is_file():
                    with open(config_path) as f:
                        config_data = yaml.load(f, Loader=yaml.FullLoader)

                    logging.info(f"Config data retrieved successfully for ID {id}")
                    return PlainTextResponse(content=yaml.dump(config_data))
                else:
                    logging.error(f"Config file not found for ID {id}")
                    return JSONResponse(content={"error": "Config file not found"}, status_code=404)
            else:
                logging.error("Invalid secret provided")
                return JSONResponse(content={"error": "Invalid secret provided"}, status_code=401)
        else:
            logging.error("Invalid ID provided")
            return JSONResponse(content={"error": "Invalid ID provided"}, status_code=404)

    except Exception as e:
        logging.exception("Error while retrieving config data")
        return JSONResponse(content={"error": str(e)}, status_code=500)

@app.get("/api/json/get")
async def get_config(id: str = Query(...), secret: str = Query(...)):
    try:
        secrets = load_secrets()

        if id in secrets:
            secret_hash = secrets[id].encode('utf-8')
            combined_secret = id + secret
            secret_bytes = combined_secret.encode('utf-8')

            if bcrypt.checkpw(secret_bytes, secret_hash):
                config_path = Path(f"config_storage/{id}.json")

                if config_path.is_file():
                    with open(config_path) as f:
                        config_data = json.load(f)

                    logging.info(f"Config data retrieved successfully for ID {id}")
                    return JSONResponse(content=config_data)
                else:
                    logging.error(f"Config file not found for ID {id}")
                    return JSONResponse(content={"error": "Config file not found"}, status_code=404)
            else:
                logging.error("Invalid secret provided")
                return JSONResponse(content={"error": "Invalid secret provided"}, status_code=401)
        else:
            logging.error("Invalid ID provided")
            return JSONResponse(content={"error": "Invalid ID provided"}, status_code=404)

    except Exception as e:
        logging.exception("Error while retrieving config data")
        return JSONResponse(content={"error": str(e)}, status_code=500)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run("witfig:app", host="0.0.0.0")